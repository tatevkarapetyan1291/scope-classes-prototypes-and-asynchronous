//Classes
class Task {
  constructor(name) {
      this.name = name;
  }
}
class Guest {
  constructor(tasks) {
      this.tasks = tasks;
  }
  getTask(index) {
      return this.tasks[index];
  }
  createTask() {
      throw new Error("method 'createTask' is not defined");
  }
  changeType() {
      throw new Error("method 'changeType' is not defined");
  }
}

class User {
  constructor(tasks) {
      this.tasks = tasks;
  }
  getTask(index) {
      return this.tasks[index];
  }
  createTask(tasks) {
      return this.tasks.push(tasks);
  }
  changeType() {
      throw new Error("method 'changeType' is not defined");
  }
}

class Admin {
  constructor(userGuestArray) {
      this.userGuestArray = userGuestArray;
  }
  getArray() {
      return this.userGuestArray;
  }

  changeType(index) {
     let mainArray = this.userGuestArray;
     let arr = this.userGuestArray[index];
      if (mainArray[index] instanceof Guest) {
          mainArray[index] = new User(arr.tasks);

      } else if (mainArray[index] instanceof User) {
          mainArray[index] = new Guest(arr.tasks);
      }
      return mainArray;
  }
}
module.exports.Task = Task;
module.exports.Guest = Guest;
module.exports.User = User;
module.exports.Admin = Admin;