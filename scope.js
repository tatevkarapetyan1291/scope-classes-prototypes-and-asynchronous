//Number generator

var createCounter = (function(n = 0) {
    return function () {
        n += 1;
        return n;
    }
});
const counter = createCounter(44);
counter(); // 45
counter(); // 46
counter(); // 47

//Multiply

function curry(func,args,space) {
  var n  = func.length - args.length; //arguments still to come
  var sa = Array.prototype.slice.apply(args); // saved accumulator array
  function accumulator(moreArgs,sa,n) {
      var saPrev = sa.slice(0); 
      var nPrev  = n; 
      for(var i=0;i<moreArgs.length;i++,n--) {
          sa[sa.length] = moreArgs[i];
      }
      if ((n-moreArgs.length)<=0) {
          var res = func.apply(space,sa);
          sa = saPrev;
          n  = nPrev;
          return res;
      } else {
          return function () {
              return accumulator(arguments,sa.slice(0),n);
          }
      }
  }
  return accumulator([],sa,n);
}

function add (a,b,c) {
    if (arguments.length < this.add.length) {
      return curry(this.add,arguments,this);
    }
    return a+b+c;
}

function multiply (a,b,c) {
  if (arguments.length < this.mul.length) {
    return curry(this.mul,arguments,this);
  }
  return a*b*c;
}
multiply(2)(4)(6); // 48
multiply(3)(3)(3); // 27 