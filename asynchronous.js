//Strategies

const callback1  = (res) => {
  let sum = 0;
  for (let index = 0; index < res.length; index++) {
    sum += res[index];
  }
  return sum;
};
const callback2  = (res) => {
  let sum = 1;
  for (let index = 0; index < res.length; index++) {
    sum *= res[index];
  }
  return sum;
};
const w = (str, callback) => {
  const arr = str.split(" ");
  const result = arr.map((item) => {
    return item.length;
  });
  return callback(result);
};
console.log(w("a bb ccc dddd", callback1 ));//result 10
console.log(w("a bb ccc dddd", callback2 ));//result 24


//Mocker

const mocker = (arr) => {
  const delay = () => {
    return new Promise((resolve) => {
      return setTimeout(() => {
        resolve(arr);
      }, 1000);
    });
  };
  return delay;
};
 const getUsers = mocker([{ id: 1, name: "User1" }]);
getUsers().then((users) => {
   // Will fire after 1 second.
 console.log(users); // result: [{id: 1, name: 'User1'}];
});


//Summarize1

const promise1 = Promise.resolve(4);
const promise2 = new Promise((resolve) => resolve(2));
function summarize1(promise1, promise2) {
  return new Promise((resolve) => {
    return Promise.all([promise1, promise2]).then((res) => {
      let sum = 0;
      for (let index = 0; index < res.length; index++) {
        sum += res[index];
      }
      resolve(sum);
    });
  });
}
summarize1(promise1, promise2).then((sum) => {
  console.log(sum);
}); // result: 6


//Summarize2

async function summarize2(promise1, promise2) {
  const res = await Promise.all([promise1, promise2]);
  let sum = 0;
  for (let index = 0; index < res.length; index++) {
    sum += res[index];
  }
  return Promise.resolve(sum);
}
summarize2(promise1, promise2).then((sum) => {
  console.log(sum,'result');
 }); // result: 6