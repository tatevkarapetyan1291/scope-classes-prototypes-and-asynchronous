//Logger
function Logger() {
  let arr = [];
  this.log = (str) => {
    arr.push(str);
  };
  this.getLog = () => {
    console.log(arr);
  };
  this.clearLog = () => {
    arr = [];
  };
}
 const logger = new Logger();
 logger.log("Event 1");
 logger.log("Event 2");
 logger.getLog(); // ['Event 1', 'Event 2']
 logger.clearLog();
 logger.getLog(); // []

//Shuffle
Array.prototype.shuffle = function () {
  let currentIndex = this.length,
    randomIndex;
  while (currentIndex != 0) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;
    [this[currentIndex], this[randomIndex]] = [
      this[randomIndex],
      this[currentIndex],
    ];
  }
  return this;
};
[1,2,3,4].shuffle(); // result: some random shuffling ex: [2,3,4,1]
['a', 'b', 'c'].shuffle(); // result: some random shuffling ex: ['c', 'b', 'a']